# Running `titan-nf`

## TLDR - Quick Start

Step 1, create FOFN, Step 2, run `titan-nf`, Step 3, that's it!

```{bash}
titan-gc-prepare.py <YOUR_FASTQS> <YOUR_WORKFLOW> <YOUR_PRIMERS> --tsv > samples.txt
titan-nf --samples samples.txt --run_name <YOUR_RUN_NAME>
```

- `<YOUR_FASTQS>`: the directory where your FASTQS are stored
- `<YOUR_WORKFLOW>`: the workflow to execute (*clearlabs, illumina_pe, illumina_se, or ont*)
- `<YOUR_PRIMERS>`: the file with your primers in BED format
- `<YOUR_RUN_NAME>`: the name of the sequencing run

On the otherhand, if you want to learn more about the process for running `titan-nf` please keep reading!

!!! tip "Check `/opt/titan/data` for primers"
    Some of the common primers used for SARS-CoV-2 seqeuncing are available in `/opt/titan/data/`

!!! danger "Please make `<YOUR_RUN_NAME>` informative"
    It's very important for downstream back up that your run names are informative and unique. If in doubt
    use the name given to the run before sequencing.

## Run `titan-nf`

With your _handy dandy_ FOFN, you have everything you need to run your samples though `titan-nf`.

```{bash}
titan-nf --samples samples.txt --run_name test_run
N E X T F L O W  ~  version 21.04.0
Launching `/home/robert_petit/repos/titan-nf/main.nf` [gigantic_legentil] - revision: 69419d039a
executor >  local (3)
[d8/628d96] process > update_pangolin [100%] 1 of 1 ✔
[fc/a123a2] process > titan_gc (test) [100%] 1 of 1 ✔
[a3/e6f0a1] process > merge_results   [100%] 1 of 1 ✔

    Titan-NF Execution Summary
    ---------------------------
    Command Line    : nextflow run /home/robert_petit/repos/titan-nf/main.nf --samples samples.txt --run_name test_run
    Resumed         : true
    Completed At    : 2021-08-05T21:02:36.953970Z
    Duration        : 4m 12s
    Success         : true
    Exit Code       : 0
    Error Report    : -
    Launch Dir      : /home/robert_petit/repos/titan-nf/test
    Working Dir     : /home/robert_petit/repos/titan-nf/test/work

Deleting 'work' directory (Total size was 2.062G)
Completed at: 05-Aug-2021 21:02:37
Duration    : 4m 13s
CPU hours   : 0.3
Succeeded   : 4
```

And, we're all set (hopefully for you too!) This will leave you with lots out outputs. To learn more about the outputs, please check out the [Output Overview](/titan-nf/output-overview/).

## `titan-nf` Parameters

```{bash}
Required Parameters:
    --samples FILE          File produced by titan-prepare.py (--tsv used)

    --run_name STR          Name to use for the results folder

Optional Parameters:
    --titan_opts FILE       A JSON file with Titan GC parameters to use.
                                Default: Use Titan-GC's defaults

    --pangolin_docker STR   The pangolin docker image to use
                                Default: Use Titan-GC's defaults

    --profile STR           Profile for Titan GC to use
                                Options: docker or singularity
                                Default: singularity

    --singularity_cache STR
                            Location where Singularity images are saved to
                                Default: /opt/singularity/cache/titan/

    --outdir DIR            Directory to write results to
                                Default: ./

    --max_retry INT         Maximum number of times to retry a failed job
                                Default: 5

    --keep_files            Prevent removal of the "work" directory after
                                a successful run.
```
