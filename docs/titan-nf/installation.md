# Installation

Currently the `titan-nf` is installable from my personal Anaconda channel, [rpetit3](https://anaconda.org/rpetit3/titan-nf). There are plans to add it to [Bioconda](https://bioconda.github.io/), but that will require some other packages to make their way to Bioconda before I can get `titan-nf` added.

## Create `titan-nf` Environment

To install `titan-nf` we'll want to create a new environment. 

```{bash}
conda create -n titan-nf -c rpetit3 -c conda-forge -c bioconda titan-nf
conda activate titan-nf
```

!!! tip "Keep `base` environment clean"
    Conda is a very senstive when it comes to the `base` environment. So it's a good practice to limit what's installed in the `base` environment. I personally only install `mamba` in the `base` environment. Don't fret though! Its easy enough to reinstall Conda incase things break, but its much quicker to delete an environment.

## Update `titan-nf`

`titan-nf` is currently under active development, so it may be necessary to update it from time to time. Here's how:

```{bash}
conda activate titan-nf
conda update -c rpetit3 -c conda-forge -c bioconda titan-nf
```

When you update `titan-nf` you should see something like this:

```{bash}
The following packages will be UPDATED:
  titan-nf                                          1.0.0-0 --> 1.0.1-0
```

This is telling you that `titan-nf` is going to be updated from `v1.0.0` to `v1.0.1`.

## Removing `titan-nf`

Incase the need ever arises, and you need to remove the `titan-nf` environment, it's _easy peasy_:

```{bash}
conda env remove -n titan-nf
```

Conda will very quietly delete the environment, and you should no longer see it when you run `conda env list`.
